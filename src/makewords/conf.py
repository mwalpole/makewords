import os

ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), "."))
NLTK_DIR = os.path.join(ROOT_DIR, "nltk_data")
